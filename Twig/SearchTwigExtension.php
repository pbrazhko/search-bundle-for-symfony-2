<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 23.06.15
 * Time: 16:25
 */

namespace CMS\SearchBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;

class SearchTwigExtension extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    private $container;

    function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('cms_search_filter', [$this, 'searchFilter'], ['is_safe' => ['html'], 'needs_environment' => true]),
        );
    }

    public function searchFilter(\Twig_Environment $environment, $type, array $defaultFieldsData = array(), $template = 'SearchBundle:Twig:filter.html.twig')
    {
        $service = $this->container->get('cms.search.service');

        $form = $service->getFilterForm($type, $defaultFieldsData);

        $viewParameters = [
            'type' => $type,
            'searchService' => $service,
            'form' => $form->createView()
        ];

        return $environment->render($template, $viewParameters);
    }

    public function getName()
    {
        return 'twig_search_extension';
    }
}
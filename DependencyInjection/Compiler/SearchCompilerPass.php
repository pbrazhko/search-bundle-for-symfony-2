<?php

namespace CMS\SearchBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class SearchCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $searchFilterProvider = $container->findDefinition('cms.search.service');

        $taggedServices = $container->findTaggedServiceIds('cms.search.provider');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $searchFilterProvider->addMethodCall(
                    'addProvider',
                    array(new Reference($id))
                );
            }
        }
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 12.01.15
 * Time: 16:49
 */

namespace CMS\SearchBundle;


use CMS\SearchBundle\DependencyInjection\Compiler\SearchCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SearchBundle extends Bundle{
    public function isEnabled(){
        return true;
    }

    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new SearchCompilerPass());
    }
}
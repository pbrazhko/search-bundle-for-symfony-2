<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 22.06.15
 * Time: 16:03
 */

namespace CMS\SearchBundle\Interfaces;

use CMS\SearchBundle\Services\SearchService;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;

interface SearchProviderInterface
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $defaultFieldsData
     * @return mixed
     */
    public function buildFilterForm(FormBuilderInterface $builder, array $defaultFieldsData = array());

    /**
     * @param Form $form
     * @param SearchService $searchService
     * @return mixed
     */
    public function search(Form $form, SearchService $searchService);

    /**
     * @return string
     */
    public function getType();
}
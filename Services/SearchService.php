<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 22.06.15
 * Time: 15:40
 */

namespace CMS\SearchBundle\Services;

use CMS\SearchBundle\Exceptions\InvalidArgumentException;
use CMS\SearchBundle\Interfaces\SearchProviderInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;

class SearchService
{
    const SEARCH_VIEW_MAP = 'map';
    const SEARCH_VIEW_GRID = 'grid';
    const SEARCH_VIEW_LIST = 'list';

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var array
     */
    private $providers = array();

    /**
     * @var FormBuilderInterface
     */
    private $form;

    /**
     * @var int
     */
    private $count = 0;

    /**
     * @var array
     */
    private $orderBy;

    /**
     * @var integer
     */
    private $limit;

    /**
     * @var integer
     */
    private $page;

    /**
     * @var string
     */
    private $view;

    /**
     * @var string
     */
    private $locale;

    public function __construct(FormFactory $formFactory, $default_locale)
    {
        $this->formFactory = $formFactory;

        $this->locale = $default_locale;
    }

    public function getSupportedViewTypes()
    {
        return [self::SEARCH_VIEW_GRID, self::SEARCH_VIEW_LIST, self::SEARCH_VIEW_MAP];
    }

    /**
     * @param null $type
     * @param array $defaultFieldsData
     * @return $this|FormBuilderInterface
     */
    public function getFilterForm($type, array $defaultFieldsData = array())
    {
        if (null !== $this->form) {
            return $this->form;
        }

        if (!array_key_exists($type, $this->providers)) {
            throw new InvalidArgumentException(sprintf('The search type of \'%s\' is not found!', $type));
        }

        $provider = $this->providers[$type];

        $data = array();

        $formBuilder = $this->formFactory->createBuilder(FormType::class, $data);

        $formBuilder
            ->setMethod('GET')
            ->add('title', TextType::class);

        $provider->buildFilterForm($formBuilder, $defaultFieldsData);

        $this->form = $formBuilder
            ->getForm()
            ->handleRequest(Request::createFromGlobals());

        return $this->form;
    }

    /**
     * @param $type
     * @return array|mixed
     */
    public function getSearchResult($type)
    {
        $result = [];

        if (!array_key_exists($type, $this->providers)) {
            throw new InvalidArgumentException(sprintf('The type of \'%s\' is not found!', $type));
        }

        $provider = $this->providers[$type];

        $form = $this->getFilterForm($type);

        $result = $provider->search($form, $this);

        $this->count = count($result);

        return $result;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     * @return $this
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     * @return $this
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @return array
     */
    public function getProviders()
    {
        return $this->providers;
    }

    /**
     * @param array $providers
     * @return $this
     */
    public function setProviders($providers)
    {
        $this->providers = $providers;

        return $this;
    }

    /**
     * @param SearchProviderInterface $repository
     *
     * @return $this
     */
    public function addProvider(SearchProviderInterface $repository)
    {
        $this->providers[strtolower($repository->getType())] = $repository;

        return $this;
    }

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     * @return SearchService
     */
    public function setOrderBy(array $orderBy = [])
    {
        $this->orderBy = $orderBy;

        return $this;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     * @return SearchService
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return SearchService
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return string
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param string $view
     * @return SearchService
     */
    public function setView($view)
    {
        if (!in_array($view, $this->getSupportedViewTypes())) {
            throw new InvalidArgumentException(sprintf('Not supported view type! %s', $view));
        }

        $this->view = $view;

        return $this;
    }
}